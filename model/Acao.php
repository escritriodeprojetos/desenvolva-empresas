<?php
include_once 'MysqliDb.php';

class Acao {

    protected $id;
    protected $endereco;
    protected $parceiroChave;
    protected $contato;
    protected $telefone;
    protected $email;
    protected $latitude;
    protected $longitude;
    protected $atividadeId;

    function __construct($id = null, $endereco = null, $parceiroChave = null, $contato = null, $telefone = null, $email = null, $latitude = null, $longitude = null, $atividadeId = null) {
        $this->id = $id;
        $this->endereco = $endereco;
        $this->parceiroChave = $parceiroChave;
        $this->contato = $contato;
        $this->telefone = $telefone;
        $this->email = $email;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->atividadeId = $atividadeId;
    }

    function getId() {
        return $this->id;
    }

    function getEndereco() {
        return $this->endereco;
    }

    function getParceiroChave() {
        return $this->parceiroChave;
    }

    function getContato() {
        return $this->contato;
    }

    function getTelefone() {
        return $this->telefone;
    }

    function getEmail() {
        return $this->email;
    }

    function getLatitude() {
        return $this->latitude;
    }

    function getLongitude() {
        return $this->longitude;
    }

    function getAtividadeId() {
        return $this->atividadeId;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEndereco($endereco) {
        $this->endereco = $endereco;
    }

    function setParceiroChave($parceiroChave) {
        $this->parceiroChave = $parceiroChave;
    }

    function setContato($contato) {
        $this->contato = $contato;
    }

    function setTelefone($telefone) {
        $this->telefone = $telefone;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    function setLongitude($longitude) {
        $this->longitude = $longitude;
    }

    function setAtividadeId($atividadeId) {
        $this->atividadeId = $atividadeId;
    }
    
    function BuscaById($id) {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();

        $this->db->where('id', $id);

        $results = $this->db->get('acao');

        if ($this->db->count > 0) {
            return $results;
        } else {
            return false;
        }
    }
    
    function BuscaByAtividadeId($idAtividade) {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();

        $this->db->where('atividade_id', $idAtividade);

        $results = $this->db->get('acao');

        if ($this->db->count > 0) {
            return $results;
        } else {
            return false;
        }
    }

    function CadastraAcao() {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();

        $insertData = Array(
            'endereco' => $this->endereco,
            'parceiro_chave' => $this->parceiroChave,
            'contato' => $this->contato,
            'telefone' => $this->telefone,
            'email' => $this->email,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'atividade_id' => $this->atividadeId,
        );

        $this->db->insert('acao', $insertData);
        
        return $this->db->getInsertId();
    }
    
    function DeletaByAtividadeId($atividadeId) {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();
        
        $this->db->where('atividade_id', $atividadeId);

        return ($this->db->delete('acao') ? true : false);
    }
    
    function AtualizaAcao($id) {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();
        
        $dados = Array(
            'endereco' => $this->endereco,
            'parceiro_chave' => $this->parceiroChave,
            'contato' => $this->contato,
            'telefone' => $this->telefone,
            'email' => $this->email,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'atividade_id' => $this->atividadeId,
        );
        
        $this->db->where('id', $this->id);
                
        $this->db->update('acao', $dados);  
    }
    
}
