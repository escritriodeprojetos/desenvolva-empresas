<?php
include_once 'MysqliDb.php';

/**
 * Classe Usuario
 *
 * @author Zilly
 */
class Usuario {

    protected $id;
    protected $nome;
    protected $email;
    protected $senha;
    protected $dataCriacao;
    protected $ativo;
    protected $db;

    function __construct($nome = null, $email = null, $senha = null) {
        $this->nome = $nome;
        $this->email = $email;
        $this->senha = $senha;
    }

    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function getEmail() {
        return $this->email;
    }

    function getSenha() {
        return $this->senha;
    }

    function getDataCriacao() {
        return $this->dataCriacao;
    }

    function getAtivo() {
        return $this->ativo;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setSenha($senha) {
        $this->senha = $senha;
    }

    function setDataCriacao($dataCriacao) {
        $this->dataCriacao = $dataCriacao;
    }

    function setAtivo($ativo) {
        $this->ativo = $ativo;
    }

    //Verifica se já existe no banco de dados um usuário com a matrícula e senha informadas no formulário de login
    function Login($email, $senha) {
        //$senha ja esta com md5

        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();
        
        $this->db->where('usuario_email', $email);
        $this->db->where('usuario_senha', $senha);        
        //SELECT * FROM usuario WHERE matricula_siape = ? AND senha = ?

        $results = $this->db->get('usuario');

        //só deve haver no máximo um usuário com a mesma matrícula e senha
        if ($this->db->count === 1) {
            foreach ($results as $result) {
                $this->id = $result['usuario_id'];
                $this->nome = $result['usuario_nome'];
                $this->email = $result['usuario_email'];
                $this->senha = $result['usuario_senha'];
                $this->dataCriacao = $result['usuario_data_criacao'];
                $this->ativo = $result['usuario_ativo'];
            }
            return true;
        } else {
            return false;
        }
    }
   
    //Verifica se determinado e-mail ja está cadastrado no banco
    function VerificaEmail($email) {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();

        $this->db->where('usuario_email', $email);

        $this->db->get('usuario');

        if ($this->db->count === 1) {
            return true;
        } else {
            return false;
        }
    }
    
    public function EnviarEmailAtivacaoContaNew() {

        $to = $this->email;
        $subject = "Portal Desenvolva - Ativação de conta";
        
        $htmlContent = '<html>
            <head>
                <title>Bem vindo(a) ao Portal Desenvolva </title>
            </head>
            <body>
                <h1>Obrigado por se cadastrar no Portal Desenvolva!</h1>
                <p>O Portal Desenvolva é uma iniciativa da Pró-Reitoria de Extensão com o apoio do Escritório de Projetos - EcoAGILE.  Por meio desta ferramenta, objetiva-se mapear as potencialidades existentes nos projetos da UFSM.</p>
                <p>Para ativar sua conta, clique no link abaixo:</p>
                <p><a href="http://www.ecoagile.com.br/desenvolva/util/ativa.php?dados=' . md5(date('d-m-y')) . $this->email . '">Ativar Conta</a></p>
                <p>Cordialmente,</p>
                <p><b>Equipe EcoAGILE.</b></p>
            </body>
        </html>';
        
        // Set content-type header for sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        
        // Additional headers
        $headers .= 'From: EcoAGILE - Escritório de Projetos<contato@ecoagile.com.br>' . "\r\n";
        $headers .= 'Bcc: miguelbrasil@ctism.ufsm.br' . "\r\n";
        
        // Send email
        if(mail($to,$subject,$htmlContent,$headers)):
            $successMsg = 'Email enviado com sucesso.';
        else:
            $errorMsg = 'Falha ao enviar email.';
        endif;
    }
    //Cadastra um novo usuário no banco com os dados do formulário passados por post
    function CadastraUsuario() {
       
        if ($this->VerificaEmail($this->email) == false) {
            
            $iniArray = parse_ini_file('config.ini');

            $this->db = new MysqliDb($iniArray);

            $this->db->connect();

            $insertData = Array(
                'usuario_nome' => $this->nome,
                'usuario_email' => $this->email,
                'usuario_senha' => $this->senha,
                'usuario_ativo' => 0
            );

            $this->db->insert('usuario', $insertData);

            $this->EnviarEmailAtivacaoContaNew();
            echo 'ta aquiiii';
            return true;
           // header('location:../cadastroUsuario.php?msg=sucesso&email=' . $this->email);
        } else {
            header('location:../cadastroUsuario.php?msg=email');
        }
    }
    
    //Busca o usuário no bando de dados com o id passado por parâmetro
    function BuscaById($id) {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();

        $this->db->where('id', $id);
        //SELECT * FROM usuario WHERE matricula_siape = ? AND senha = ?

        $results = $this->db->get('usuario');

        //só deve haver no máximo um usuário com a mesma matrícula e senha
        if ($this->db->count > 0) {
            foreach ($results as $result) {
                $this->id = $result['usuario_id'];
                $this->nome = $result['usuario_nome'];
                $this->email = $result['usuario_email'];
                $this->senha = $result['usuario_senha'];
                $this->dataCriacao = $result['usuario_data_criacao'];
                $this->ativo = $result['usuario_ativo'];
            }
            return true;
        } else {
            return false;
        }
    }

    //Troca no banco o valor do campo 'ativo' da tabela usuário para 1
    function AtivarConta() {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();
        $dados = Array(
            'ativo' => 1
        );
        
        $this->db->where('usuario_email', $this->email);
                
        $this->db->update('usuario', $dados);
    }
}
