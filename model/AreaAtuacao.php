<?php

include_once 'MysqliDb.php';

/**
 * Description of Atividade
 *
 * @author Tots
 */
class AreaAtuacao {

    protected $id;
    protected $areaId;
    protected $projetoId;
    protected $db;

    function __construct($id = null, $nome = null, $projetoId = null) {
        $this->id = $id;
        $this->nome = $nome;
        $this->projetoId = $projetoId;
    }

    function getId() {
        return $this->id;
    }

    function getProjetoId() {
        return $this->projetoId;
    }

    function getAreaId() {
        return $this->areaId;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setProjetoId($projetoId) {
        $this->projetoId = $projetoId;
    }

    function setAreaId($areaId) {
        $this->areaId = $areaId;
    }

    function CadastraArea() {
        if (!$this->VerificaArea($this->areaId, $this->projetoId)) {

            $iniArray = parse_ini_file('config.ini');

            $this->db = new MysqliDb($iniArray);

            $this->db->connect();

            $insertData = Array(
                'area_id' => $this->areaId,
                'projeto_id' => $this->projetoId
            );

            $this->db->insert('area_atuacao', $insertData);

            return true;
        } else {
            return false;
        }
    }

    private function VerificaArea($areaId, $projetoId) {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();

        $this->db->where('area_id', $areaId, '=', 'AND');
        $this->db->where('projeto_id', $projetoId);

        $results = $this->db->get('area_atuacao');

        if ($this->db->count === 1) {
            foreach ($results as $result) {
                $this->setId($result['id']);
            }
            return true;
        } else {
            return false;
        }
    }

    function ExcluirAreaAtuacao($id) {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();

        $this->db->where('id', $id);

        $this->db->delete('area_atuacao');

        if ($this->db->count === 1) {
            return false;
        } else {
            return true;
        }
    }

    function BuscaByProjetoId($projetoId) {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();

        $this->db->where('projeto_id', $projetoId);

        $results = $this->db->get('area_atuacao');

        if ($this->db->count > 0) {
            return $results;
        } else {
            return false;
        }
    }

    function BuscaByProjetoIdArray($projetoId) {
        $results = $this->BuscaByProjetoId($projetoId);

        if ($results) {
            $result = array();
            foreach ($results as $area) {
                array_push($result, $area['area_id']);
            }
            return $result;
        } else {
            return false;
        }
    }

    function ExcluirAreaAtuacaoByProjetoId($projetoId) {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();

        $this->db->where('projeto_id', $projetoId);

        $this->db->delete('area_atuacao');

        if ($this->db->count === 1) {
            return false;
        } else {
            return true;
        }
    }
    
}
