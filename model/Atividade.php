<?php

include_once 'MysqliDb.php';

/**
 * Description of Atividade
 *
 * @author Zilly
 */
class Atividade {

    protected $id;
    protected $nome;
    protected $projetoId;
    protected $db;

    function __construct($id = null, $nome = null, $projetoId = null) {
        $this->id = $id;
        $this->nome = $nome;
        $this->projetoId = $projetoId;
    }

    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function getProjetoId() {
        return $this->projetoId;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setProjetoId($projetoId) {
        $this->projetoId = $projetoId;
    }

    function CadastraAtividade() {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();

        $insertData = Array(
            'nome' => $this->nome,
            'projeto_id' => $this->projetoId
        );

        $this->db->insert('atividade', $insertData);
    }

    function BuscaByProjetoId($projetoId) {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();

        $this->db->where('projeto_id', $projetoId);

        $results = $this->db->get('atividade');

        if ($this->db->count > 0) {
            return $results;
        } else {
            return false;
            //return $this->db->get('atividade');
        }
    }

    function ExcluiAtividade($id) {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();

        $this->db->where('id', $id);

        $this->db->delete('atividade');
    }

    function VerificaDados($id) {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();

        $this->db->where('id', $id);

        $this->db->get('atividade');

        if ($this->db->count === 1) {
            return true;
        } else {
            return false;
        }
    }

    function BuscaByProjetoIdByNome() {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();
        $this->db->where('nome', $this->nome, '=', 'AND');
        $this->db->where('projeto_id', $this->projetoId);

        $results = $this->db->get('atividade');

        if ($this->db->count > 0) {
            foreach ($results as $result) {
                $this->setId($result['id']);
                $this->setNome($result['nome']);
            }
            return true;
        } else {
            return false;
        }
    }

    function DeleteByProjetoId($projetoId) {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();

        $this->db->where('projeto_id', $projetoId);

        $this->db->delete('atividade');
    }

    function AtualizaAtividade() {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();
        
        $dados = Array(
            'nome' => $this->nome
        );
        
        $this->db->where('id', $this->id);
                
        $this->db->update('atividade', $dados);
    }

    function BuscaByNome($nome) {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();
        $this->db->where('nome', $nome);

        $results = $this->db->get('atividade');

        if ($this->db->count > 0) {
            foreach ($results as $result) {
                $this->id = $result['id'];
                $this->nome = $result['nome'];
            }
            return true;
        } else {
            return false;
        }
    }

}
