<?php
/**
 * Description of Area
 *
 * @author Bruno
 */
class Area {
    protected $id;
    protected $nome;
    
    function getNome() {
        return $this->nome;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function __construct($id=null, $nome=null) {
        $this->id = $id;
        $this->nome = $nome;
    }

    function BuscaTodos() {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();
        
        $results = $this->db->get('area');
        
        if ($this->db->count > 0) {
            return $results;
        } else {
            return false;
        }
    }
    
}
