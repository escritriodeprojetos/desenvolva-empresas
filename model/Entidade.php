<?php

include_once 'MysqliDb.php';
include_once 'Atividade.php';
include_once 'Acao.php';
include_once 'AreaAtuacao.php';

/**
 * Classe Projeto
 *
 * @author Bruno Frizzo
 */
class ParceiroChave {

    protected $id;
    protected $nome;
    protected $usuarioId;
    protected $db;

    function __construct($id = null, $nome = null, $usuarioId = null) {
        $this->id = $id;
        $this->nome = $nome;
        $this->usuarioId = $usuarioId;
    }

    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }
    
    function getUsuarioId() {
        return $this->usuarioId;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setUsuarioId($usuarioId) {
        $this->usuarioId = $usuarioId;
    }

    function CadastraParceiroChave() {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();

        $insertData = Array(
            'parceiro_chave_nome' => $this->nome,
            'usuario_id' => $this->usuarioId,
        );
        return $this->db->insert('parceiro_chave', $insertData);
    }

    function BuscaById($id) {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();

        $this->db->where('parceiro_chave_id', $id);

        $results = $this->db->get('parceiro_chave');

        if ($this->db->count > 0) {
            foreach ($results as $result) {
                $this->id = $result['parceiro_chave_id'];
                $this->nome = $result['parceiro_chave_nome'];
                $this->usuarioId = $result['usuario_id'];
            }
            return true;
        } else {
            return false;
        }
    }

    function BuscaByUsuario($usuarioId) {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();

        $this->db->where('usuario_id', $usuarioId);

        $results = $this->db->get('parceiro_chave');

        if ($this->db->count > 0) {
            return $results;
        } else {
            return false;
        }
    }
    
    function AtualizarParceiroChave($id, $nomeParceiroChave) {
        $iniArray = parse_ini_file('config.ini');

        $this->db = new MysqliDb($iniArray);

        $this->db->connect();
        
        $this->BuscaById($id);
        
        $data = Array(
            'parceiro_chave_nome' => $nomeParceiroChave,
            'usuario_id' => $this->getUsuarioId()
        );
        
        $this->db->where('parceiro_chave_id', $id);
         
        $this->db->update('parceiro_chave', $data);
        
        return true;
    }
    
}
