<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <script src="script.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen"> 
        <link href="../css/dataTables.bootstrap.min.css" rel="stylesheet" media="screen">
        <script type="text/javascript" src="../js/externo/jquery-1.12.0.min.js"></script>
        <script type="text/javascript" src="../js/externo/bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/externo/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../js/externo/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="js/externo/plugin-mask.js"></script>
    </head>
    <body>
        <?php
        include_once '../model/Usuario.php';
        if (!empty($_GET['dados'])) {
            $siape = substr($_GET['dados'], -7);
            $data = substr($_GET['dados'], 0, -7);
            if ($data === md5(date('d-m-y'))) {
                $usuario = new Usuario($nome = null, $email = null, $siape);
                $usuario->AtivarConta();
                header('location:../index.php?msg=contaAtivada');
            } else {
                header('location../cadastroUsuario.php?msg=expirado&siape=' . $siape);
            }
        }
        ?>
    </body>
</html>
