<?php
ob_start();
session_start();
include_once '../model/Usuario.php';

if (!empty($_POST['siape']) && !empty($_POST['senha'])) {
    $usuario = new Usuario();
    if ($usuario->Login($_POST['siape'], md5($_POST['senha']))) {
        if ($usuario->getAtivo() === 0) {
            header('Location: ../index.php?msg=ativo');
        } else {
            $_SESSION['usuario_id'] = $usuario->getId();
            $_SESSION['usuario_nome'] = $usuario->getNome();
            $_SESSION['usuario_siape'] = $usuario->getSiape();
            $_SESSION['usuario_senha'] = $usuario->getSenha();
            header('Location: ../entidades.php');
        }
    } else {
        header('Location: ../index.php?loginerror=1');
    }
}
