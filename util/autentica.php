<?php
session_start();
include_once 'model/Usuario.php';
$usuario = new Usuario();
if(!empty($_SESSION)){
    if(!$usuario->Login($_SESSION['usuario_siape'], $_SESSION['usuario_senha'])){
        header('Location: index.php');
    }
} else {
    header('Location: index.php');
}
