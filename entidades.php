<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <?php
        include_once 'util/autentica.php';
        include_once 'template/scripts.php';
        include_once 'model/Entidade.php';
        ?>
        <title>Entidades - Portal Desenvolva</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-default navbar-fixed-top">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <span class="navbar-brand">Portal Desenvolva</span>
                            </div>
                            <div class="collapse navbar-collapse" id="myNavbar">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="#" id="creditosLink" data-toggle="modal" data-target="#creditos"><span class="glyphicon glyphicon-info-sign gi-2-1x"></span> Créditos</a></li>
                                    <li><a href="util/logout.php"><span class="glyphicon glyphicon-log-out"></span> Sair</a></li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            <fieldset style="margin-top: 75px">
                <?php
                    $projeto = new Entidade();
                    $projetos = $projeto->BuscaByUsuario($_SESSION['usuario_id']);
                    if (empty($projetos)) {
                        echo '<div class="alert alert-info" id="msg">'
                        . '<button type="button" class="close" data-dismiss="alert">×</button>Nenhum projeto cadastrado. Clique no botão "<span class="glyphicon glyphicon-plus-sign"></span>" para adicionar um novo projeto.</div>';
                    }
                ?>
                <div id="modalCreditos">
                    <?php
                        include_once 'template/modalCreditos.php';
                    ?>
                </div>
                <legend>Entidades <a href="#" class="glyphicon glyphicon-plus-sign" id="botaoAddProjeto" style="text-decoration: none; font-size: 1.1em !important;"></a><span id="msgEdicao" style="color: #ff3333"></span></legend>
                <div id="avisos"></div>
                <div class="row" id="formProjeto"></div>               
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <table id="entidades" class="table table-striped table-bordered table-condensed table-hover display nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Número</th>
                                    <th>Nome</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($projetos)) {
                                    foreach ($projetos as $result) {
                                        echo '<tr>'
                                        . '<td>' . $result['numero'] . '</td>'
                                        . '<td>' . $result['nome'] . '</td>'
                                        . '<td class="text-center" style="display: inline-table"><a class="btn btn-primary btn-sm glyphicon glyphicon-globe" data-toggle="tooltip" data-title="Identificar Projeto" href="identificacao.php?numeroProjeto=' . $result['numero'] . '" style="font-size: 1.1em !important; text-decoration: none; border-radius: 5px 0px 0px 5px;"></a>'
                                        . ' <a id="' . $result['numero'] . '" style="font-size: 1.1em !important; text-decoration: none; border-radius: 0px;" class="btn btn-danger btn-sm glyphicon glyphicon-remove" href="#" data-toggle="tooltip" data-title="Remover Projeto"></a>'
                                        . ' <a href="#" class="btn btn-warning btn-sm glyphicon glyphicon-pencil" style="font-size: 1.1em !important; border-radius: 0px 5px 5px 0px; text-decoration: none;" data-toggle="tooltip" data-title="Editar Projeto"></a><input type="hidden" value="' . $result['id'] . '"></td>'
                                        . '</tr>';
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </fieldset>
        </div>
        <script src="js/entidades.php.js"></script>
    </body>
</html>
