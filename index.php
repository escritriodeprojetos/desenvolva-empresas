<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/style.css" rel="stylesheet" media="screen">
        <script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <title>Login - Portal Desenvolva</title>
    </head>
    <body class="index">
        <div class="container">
            <div class="row">
                <div class="elementos">
                    <div class="col-sm-10 col-sm-offset-1 col-md-4 col-md-offset-1">
                        <section class="login-form">
                            <form method="post" action="util/login.php" role="login">
                                <!--<img src="http://i.imgur.com/RcmcLv4.png" class="img-responsive" alt="" />-->
                                <h3 style="text-align: center">Acesso ao Portal Desenvolva</h3>
                                <input min="7" maxlength="7" type="text" name="siape" placeholder="Matrícula SIAPE" class="form-control input-lg" required/>
                                <input type="password" name="senha"  placeholder="Senha" class="form-control input-lg" required/>
                                <?php
                                if (!empty($_GET['loginerror']) && $_GET['loginerror'] === '1') {
                                    echo '<span style="color: #ff0000; font-family: verdana;">Erro ao logar!</span>';
                                }
                                if (!empty($_GET['msg']) && $_GET['msg'] === 'ativo') {
                                    echo '<span style="color: #ff0000; font-family: verdana;">A conta precisa ser ativada</span>';
                                }
                                if (!empty($_GET['msg']) && $_GET['msg'] === 'contaAtivada') {
                                    echo '<span style="color: #337ab7; font-family: verdana;">Conta ativada com sucesso! <br> Você já pode fazer login usando sua senha e SIAPE. </span>';
                                }
                                ?>
                                <button type="submit" name="go" class="btn btn-lg btn-primary btn-block">Entrar</button>
                            </form>
                        </section>  
                    </div>
                    <div class="col-sm-10 col-sm-offset-1 col-md-5 col-md-offset-0">
                        <section class="div-cadastro"> 
                            <h3 style="text-align: center;">Olá, <b>Coordenador de Projetos</b>, é sua primeira visita aqui?</h3>
                            <i>Para ter acesso completo ao sistema, realize um cadastro.</i><br>
                            <a href="cadastroUsuario.php" class="btn btn-lg btn-primary">Cadastrar-se</a>
                        </section>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-links">
                    <div class="col-sm-12">
                        <span>Copyright by</span>
                        <a href="http://www.escritoriodeprojetos.ufsm.br">www.escritoriodeprojetos.ufsm.br</a>
                        <span>& CR Campeiro</span>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
