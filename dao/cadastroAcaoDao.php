<?php
ob_start();
session_start();
include_once '../model/Acao.php';

if (!empty($_POST['idAtividade']) && !empty($_POST['parceiroChave']) && !empty($_POST['contato']) &&
        !empty($_POST['telefone']) && !empty($_POST['email']) && !empty($_POST['endereco']) &&
        !empty($_POST['latitude']) && !empty($_POST['longitude']) && !is_numeric($_POST['idAcao'])) {
    $acao = new Acao();
    $acao->setAtividadeId($_POST['idAtividade']);
    $acao->setParceiroChave($_POST['parceiroChave']);
    $acao->setContato($_POST['contato']);
    $acao->setTelefone($_POST['telefone']);
    $acao->setEmail($_POST['email']);
    $acao->setEndereco($_POST['endereco']);
    $acao->setLatitude($_POST['latitude']);
    $acao->setLongitude($_POST['longitude']);
    $acao->setId($acao->CadastraAcao());
    if ( $acao->getId() > 0) {
        $acoes = $acao->BuscaByAtividadeId($_POST['idAtividade']);
        foreach ($acoes as $result) {
            echo '<tr>  
                <td style="width: 30%; ">' . $result['parceiro_chave'] . '</td>  
                <td style="width: 30%; ">' . $result['contato'] . '</td>  
                <td style="width: 30%; ">' . $result['telefone'] . '</td>  
                <td style="width: 10%; "><button onclick="EditarAcao(' . $result['id'] . ')" class="btn btn-primary btn-sm glyphicon glyphicon-search botao-edita-acao" data-toggle="tooltip" data-title="Selecionar ação"></button></td>  
                <input type="hidden" value="' . $result['id'] . '" class="input-id-acao">
            </tr>';
        }
    } else {
        echo 'erro';
    }
    die();
} else if (!empty($_POST['idAcao'])) {
    $acao = new Acao();
    $acao->setContato($_POST['contato']);
    $acao->setEmail($_POST['email']);
    $acao->setEndereco($_POST['endereco']);
    $acao->setLatitude($_POST['latitude']);
    $acao->setLongitude($_POST['longitude']);
    $acao->setParceiroChave($_POST['parceiroChave']);
    $acao->setTelefone($_POST['telefone']);
    $acao->setAtividadeId($_POST['idAtividade']);
    $acao->setId($_POST['idAcao']);
    $acao->AtualizaAcao($_POST['idAcao']);
    
    if ($acao->BuscaById($_POST['idAcao']) === false) {
        echo 'erro';
    }else{
        $acao = new Acao();
        $acoes = $acao->BuscaByAtividadeId($_POST['idAtividade']);
        foreach ($acoes as $result) {
            echo '<tr>  
                <td style="width: 30%; ">' . $result['parceiro_chave'] . '</td>  
                <td style="width: 30%; ">' . $result['contato'] . '</td>  
                <td style="width: 30%; ">' . $result['telefone'] . '</td>  
                <td style="width: 10%; "><button onclick="EditarAcao(' . $result['id'] . ')" class="btn btn-primary btn-sm glyphicon glyphicon-search botao-edita-acao" data-toggle="tooltip" data-title="Selecionar ação" ></button></td>  
                <input type="hidden" value="' . $result['id'] . '" class="input-id-acao">
            </tr>';
        }
    }
     die();
}

if (!empty($_POST['idAtividade'])) {
    $acao = new Acao();
    $acoes = $acao->BuscaByAtividadeId($_POST['idAtividade']);
    
    if ($acoes === false) {
        echo '<div class="panel panel-default">
        <div class="panel-heading clearfix">
            <i class="icon-calendar"></i>   
            <h3 class="panel-title">Ações    
            <button class="glyphicon glyphicon-plus-sign" id="botaoAddAcao" style="text-decoration: none; color: #337ab7; font-size: 1.1em !important; background: transparent !important; border: 0px; padding: 0px;"></button></h3>  
        </div>  
        <div class="panel-body">  
            <div id="formAcao" style="padding-bottom: 3%;"></div>
            <table id="tabelaAcaoDados" cellpadding="0" class="table table-striped" cellspacing="0" border="0" style="text-align: left;">  
                <thead> 
                    <tr>  
                        <th style="width: 30%; ">Parceiro-chave</th>  
                        <th style="width: 30%; ">Contato</th>  
                        <th style="width: 30%; ">Telefone</th>  
                        <th style="width: 10%; "></th>  
                    </tr>   
                </thead>
                <tbody id="tabelaAcaoDadosCorpo">
                    <tr>    
                        <td colspan="4"><p style="text-align:center; font-size: 13pt;">Nenhuma ação cadastrada até o momento</p></td>   
                    </tr>
                </tbody>
            </table>  
        </div>
    </div>';
    } else {
    echo '<div class="panel panel-default">
        <div class="panel-heading clearfix">
            <i class="icon-calendar"></i>   
            <h3 class="panel-title">Ações    
            <button class="glyphicon glyphicon-plus-sign" id="botaoAddAcao" style="text-decoration: none; color: #337ab7; font-size: 1.1em !important; background: transparent !important; border: 0px; padding: 0px;" data-toggle="tooltip" data-title="Adicionar nova ação"></button><span id="editandoAcao" style="color: red"> - Em edição</span></h3>  
        </div>  
        <div class="panel-body">  
            <div id="formAcao" style="padding-bottom: 3%;"></div>
            <table id="tabelaAcaoDados" cellpadding="0" class="table table-striped" cellspacing="0" border="0" style="text-align: left;">  
                <thead> 
                    <tr>  
                        <th style="width: 30%; ">Parceiro-chave</th>  
                        <th style="width: 30%; ">Contato</th>  
                        <th style="width: 30%; ">Telefone</th>  
                        <th style="width: 10%; "></th>  
                    </tr>   
                </thead>
                <tbody id="tabelaAcaoDadosCorpo">';
                    foreach ($acoes as $result) {
                        echo '<tr> 
                            <td style="width: 30%; ">' . $result['parceiro_chave'] . '</td>  
                            <td style="width: 30%; ">' . $result['contato'] . '</td>  
                            <td style="width: 30%; ">' . $result['telefone'] . '</td>  
                            <td style="width: 10%; "><button class="btn btn-primary btn-sm glyphicon glyphicon-search botao-edita-acao" onclick="EditarAcao(' . $result['id'] . ')" data-toggle="tooltip" data-title="Selecionar ação" ></button></td>  
                            <input type="hidden" value="' . $result['id'] . '" class="input-id-acao">
                        </tr>';
                    }
                echo '</tbody>
            </table>  
        </div>
    </div>';
    die();
    }
  
}
if (!empty($_GET['idAcao'])) {
    $acao = new Acao();
    $acaoSelecionada = $acao->BuscaById($_GET['idAcao']);
    foreach ($acaoSelecionada as $result) {
        $dados[] = array(
            'endereco' => $result['endereco'],
            'parceiro_chave' => $result['parceiro_chave'],
            'contato' => $result['contato'],
            'telefone' => $result['telefone'],
            'email' => $result['email'],
            'latitude' => $result['latitude'],
            'longitude' => $result['longitude'],
            'atividade_id' => $result['atividade_id'],
            );
    }
    echo json_encode($dados);
    die();
}
