<?php
ob_start();
session_start();
include_once '../model/Entidade.php';

if (!empty($_POST['numeroProjeto']) && !empty($_POST['nomeProjeto']) && empty($_POST['idProjeto']) && !empty($_POST['tipo'])) {
    $projeto = new Entidade();
    $projeto->setNome($_POST['nomeProjeto']);
    $projeto->setNumero($_POST['numeroProjeto']);
    $projeto->setTipo($_POST['tipo']);
    $projeto->setUsuarioId($_SESSION['usuario_id']);
    $idProjeto = $projeto->CadastraEntidade();
    if($idProjeto === false) {
        echo 'erro';
    } else {
        echo $idProjeto;
    }
    die();
} else if(!empty($_POST['idProjeto']) && !empty($_POST['numeroProjeto']) && !empty($_POST['nomeProjeto']) && !empty($_POST['tipo'])) {
    $projeto = new Entidade();
    $projeto->AtualizarEntidade($_POST['idProjeto'], $_POST['numeroProjeto'], $_POST['nomeProjeto'], $_POST['tipo']);
    $projeto->BuscaById($_POST['idProjeto']);
    if ($projeto->BuscaByNumero($projeto->getNumero()) === false) {
        echo 'erro';
    }else{
        echo $_POST['idProjeto'];
    }
    die();
} else if (!empty($_GET['cmd']) && $_GET['cmd'] === 'excluir' && !empty($_GET['numeroProjeto'])) {
    $projeto = new Entidade();
    $numProjeto = $_GET['numeroProjeto'];
    if ($projeto->DeleteByNumero($numProjeto) === true) {
        echo 'sucesso';
    }else {
        echo 'erro';
    }
    die();
} else if(!empty($_POST['idProjeto']) && !empty($_POST['cmd']) && $_POST['cmd'] === 'tipo') {
    $projeto = new Entidade();
    $projeto->BuscaById($_POST['idProjeto']);
    echo $projeto->getTipo();
}
 