<?php
ob_start();
session_start();
include_once '../model/Usuario.php';

if (!empty($_POST['nome']) && !empty($_POST['email']) && !empty($_POST['senha']) && !empty($_POST['senha2'])) {
    if ($_POST['senha'] === $_POST['senha2']) {
        $usuario = new Usuario();
        $usuario->setNome($_POST['nome']);
        $usuario->setEmail($_POST['email']);
        $usuario->setSenha(md5($_POST['senha']));
        if($usuario->CadastraUsuario() === true){
            if($usuario->VerificaEmail($_POST['email']) === true){
                header('location:../cadastroUsuario.php?msg=sucesso&email=' . $_POST['email']);
            }
        }
    } else {
        header('location: ../cadastroUsuario.php?msg=senha');
    }
}  
