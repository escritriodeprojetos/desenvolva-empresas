<?php
ob_start();
session_start();
include_once '../model/Projeto.php';
include_once '../model/AreaAtuacao.php';

if (!empty($_POST['areaAtuacao']) && !empty($_POST['numeroProjeto'])) {
    $projeto = new Projeto();
    $areaObj = new AreaAtuacao();
    if (!$projeto->BuscaByNumero($_POST['numeroProjeto'])) {
        echo 'erro';
    }else {
        $projeto->BuscaByNumero($_POST['numeroProjeto']);
        $idProjeto = $projeto->getId();
        $areaObj->setAreaId($_POST['areaAtuacao']);
        $areaObj->setProjetoId($idProjeto);
        if ($areaObj->CadastraArea()) {
            echo 'sucesso';
        }else {
            if (!$areaObj->ExcluirAreaAtuacao($areaObj->getId())) {
                echo 'erro';
            }else {
                echo 'sucesso';
            }
        }
    }      
    die();
} 
