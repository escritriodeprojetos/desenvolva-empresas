<?php

ob_start();
session_start();
include_once '../model/Atividade.php';
include_once '../model/Acao.php';

if (!empty($_POST['nomeAtividade']) && !empty($_POST['projetoId']) && empty($_POST['idAtividade'])) {
    $atividade = new Atividade();
    $atividade->setNome($_POST['nomeAtividade']);
    $atividade->setProjetoId($_POST['projetoId']);

    if ($atividade->BuscaByNome($_POST['nomeAtividade']) === true) {
        echo 'erro';
    } else if ($atividade->BuscaByNome($_POST['nomeAtividade']) === false) {
        $atividade->CadastraAtividade();
        if ($atividade->BuscaByNome($_POST['nomeAtividade']) === true) {
            echo $atividade->getId();
        } else if ($atividade->BuscaByNome($_POST['nomeAtividade']) === false) {
            echo 'erro';
        }
    }
    die();
} else if (!empty($_POST['idAtividade'])) {
    $atividade = new Atividade();
    $atividade->setNome($_POST['nomeAtividade']);
    $atividade->setId($_POST['idAtividade']);
    $atividade->AtualizaAtividade();

    if ($atividade->BuscaByNome($_POST['nomeAtividade']) === true) {
        echo $atividade->getId();
    } else if ($atividade->BuscaByNome($_POST['nomeAtividade']) === false) {
        echo 'erro';
    }
    die();
} else if (!empty($_GET['id']) && !empty($_GET['cmd'])) {
    $atividade = new Atividade();
    $atividade->setId($_GET['id']);
    if ($_GET['cmd'] === 'excluir') {
        $acao = new Acao();
        $acao->DeletaByAtividadeId($_GET['id']);
        $atividade->ExcluiAtividade($_GET['id']);
        if ($atividade->VerificaDados($_GET['id']) === true) {
            echo 'erro';
        } else if ($atividade->VerificaDados($_GET['id']) === false) {
            echo $atividade->getId();
        }
    }
}
