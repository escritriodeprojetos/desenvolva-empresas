function CadastrarEntidade() {
    $('#msg').hide();
    var numeroProjeto = $('#numeroProjeto').val();
    var nomeProjeto = $('#nomeProjeto').val();
    var idProjeto = $('#idProjeto').val();
    var tipo = '';
    $('input:radio[name=tipo]').each(function() {
        if ($(this).is(':checked'))
            tipo = $(this).val();
        });
    if(nomeProjeto == '' || numeroProjeto == '' || tipo == ''){
        alert('ta entrandoa qui: ' + nomeProjeto + ' - ' + numeroProjeto + ' - ' +tipo);
        ErroSalvar();
    } else {
        $.ajax({
            type: 'POST',
            url: 'dao/cadastroEntidadeDao.php',
            data: 'numeroProjeto=' + numeroProjeto + '&nomeProjeto=' + nomeProjeto + '&tipo=' + tipo + '&idProjeto=' + idProjeto,
            success: function (data) {
                if (data === 'erro') {
                    ErroSalvar();
                } else{
                    if (idProjeto === '') {
                        AdicionarRegistro(data);
                    } else {
                        AtualizarTabela();
                        $("#botaoAddProjeto").removeClass("glyphicon glyphicon-minus-sign").addClass("glyphicon glyphicon-plus-sign");
                        $('#formProjeto').hide();
                    }
                }
            }
        });
    }
}

function ErroSalvar() {
    $('#avisos').show();
    document.getElementById("avisos").innerHTML = '<div class="alert alert-danger" id="msg">'
            + '<button type="button" class="close" data-dismiss="alert">×</button>'
            + 'Erro ao salvar entidade. Tente novamente.'
            + '</div>';
    setTimeout(function () {
        $('#avisos').hide('slow');
    }, 3000);
}

function AdicionarRegistro(idProjeto) {
    var table = $('#projetos').DataTable();
    $('input:radio[name=tipo]').each(function() {
    if ($(this).is(':checked'))
        tipo = $(this).val();
    });

    var rowNode = table.row.add([$('#numeroProjeto').val(), $('#nomeProjeto').val(), tipo, '<a class="btn btn-primary btn-sm glyphicon glyphicon-globe" data-toggle="tooltip" data-title="Identificar Entidade" href="identificacao.php?numeroProjeto=' + $('#numeroProjeto').val() + '" style="font-size: 1.1em !important; text-decoration: none; border-radius: 5px 0px 0px 5px;"></a>'
                + ' <a id="' + $('#numeroProjeto').val() + '" style="font-size: 1.1em !important; text-decoration: none; border-radius: 0px;" class="btn btn-danger btn-sm glyphicon glyphicon-remove fa-2x" href="#" data-toggle="tooltip" data-title="Remover Entidade"></a>'
                + ' <a href="#" style="font-size: 1.1em !important; text-decoration: none; border-radius: 0px 5px 5px 0px;" class="btn btn-warning bt-sm glyphicon glyphicon-pencil fa-2x" data-toggle="tooltip" data-title="Editar Entidade"></a><input type="hidden" value="' + idProjeto + '">'])
            .draw()
            .node();
    $(rowNode).find('td').eq(3).addClass('text-center');
    $(rowNode).find('td').eq(3).css({ display: "inline-table" });;

    $('[data-toggle="tooltip"]').tooltip({
        placement: 'auto top', trigger: 'hover'
    });
    LimparCampos();
    $('#avisos').show();
    
    document.getElementById("avisos").innerHTML = '<div class="alert alert-success" id="msg">'
            + '<button type="button" class="close" data-dismiss="alert">×</button>'
            + 'Entidade cadastrada com sucesso'
            + '</div>';
    setTimeout(function () {
        $('#avisos').hide('slow');
    }, 3000);
}

function ErroExclusao() {
    $('#avisos').show();
    document.getElementById("avisos").innerHTML = '<div class="alert alert-danger" id="msg">'
            + '<button type="button" class="close" data-dismiss="alert">×</button>'
            + 'Erro na exclusão da entidade.'
            + '</div>';
    setTimeout(function () {
        $('#avisos').hide('slow');
    }, 3000);
}

function EditarEntidade(idProjeto, numProjeto, nomeProjeto) {
    $('#formProjeto').show('slow');
    $("#numeroProjeto").mask('9?99999');
    $('#botaoAddProjeto').removeClass("glyphicon glyphicon-plus-sign").addClass("glyphicon glyphicon-minus-sign");
    $('#numeroProjeto').val(numProjeto);
    $('#nomeProjeto').val(nomeProjeto);
    $('#idProjeto').val(idProjeto);
    
    $.ajax({
        type: 'POST',
        url: 'dao/cadastroEntidadeDao.php',
        data: 'idProjeto=' + idProjeto + '&cmd=tipo',
        success: function (data) {
            $("#formProjeto").find("input[type=\"radio\"][value=" + data + "]").prop('checked', true);
        }
    });
    
    document.getElementById("msgEdicao").innerHTML = ' - Em edição';
}

function ErroEditar() {
    $('#avisos').show();
    document.getElementById("avisos").innerHTML = '<div class="alert alert-danger" id="msg">'
            + '<button type="button" class="close" data-dismiss="alert">×</button>'
            + 'Erro na edição do projeto.'
            + '</div>';
    setTimeout(function () {
        $('#avisos').hide('slow');
    }, 3000);
}

function AtualizarTabela() {
    $('#projetos').dataTable().fnUpdate($('#numeroProjeto').val(), $('tr.trSelecionada')[0], 0);
    $('#projetos').dataTable().fnUpdate($('#nomeProjeto').val(), $('tr.trSelecionada')[0], 1);
    $('input:radio[name=tipo]').each(function() {
    if ($(this).is(':checked'))
        tipo = $(this).val();
    });
    $('#projetos').dataTable().fnUpdate(tipo, $('tr.trSelecionada')[0], 2);
    LimparCampos();
    
    $('tr.trSelecionada').removeClass('trSelecionada');
    document.getElementById("msgEdicao").innerHTML = '';
    
    $('#avisos').show();
    document.getElementById("avisos").innerHTML = '<div class="alert alert-success" id="msg">'
            + '<button type="button" class="close" data-dismiss="alert">×</button>'
            + 'Entidade alterada com sucesso.'
            + '</div>';
    setTimeout(function () {
        $('#avisos').hide('slow');
    }, 3000);
}

function LimparCampos() {
    $("#formProjeto").find("input[type=\"radio\"]").removeProp('checked');
    $("#formProjeto").find("input[type=\"text\"]").val('');
    $("#formProjeto").find("input[type=\"number\"]").val('');
    $("#idProjeto").val('');
}

//funcao para editar projeto
$(document).on('click', '.glyphicon-pencil', function(){
        var tr = $(this).closest('tr');
        tr.addClass('trSelecionada');
        var nomeProjeto = tr.find('td').eq(1).text();
        var numProjeto = tr.find('td').eq(0).text();
        var idProjeto = tr.find('input:hidden').val();
        EditarEntidade(idProjeto, numProjeto, nomeProjeto);
});

//funcao para excluir projeto
$(document).on('click', '.glyphicon-remove', function(){
        if (confirm('Você realmente deseja excluir seu projeto? Todas as ações e atividades deste projeto também serão excluídas.')) {
            var tr = $(this).closest('tr');
            var numProjeto = $(this).attr('id');
            $.ajax({
                type: 'GET',
                url: 'dao/cadastroEntidadeDao.php',
                data: 'numeroProjeto=' + numProjeto + '&cmd=excluir',
                success: function (data) {
                    if (data === 'erro') {
                        ErroExclusao();
                    }else {
                        document.getElementById("msgEdicao").innerHTML = '';

                        LimparCampos();
                        $('#formProjeto').hide('slow');
                        $("#botaoAddProjeto").removeClass("glyphicon glyphicon-minus-sign").addClass("glyphicon glyphicon-plus-sign");
                        var table = $('#projetos').DataTable();
                        table.row(tr).remove().draw(false);
                        $('#avisos').show();
                        document.getElementById("avisos").innerHTML = 
                                '<div class="alert alert-success" id="msg">'
                                + '<button type="button" class="close" data-dismiss="alert">×</button>'
                                + 'Entidade excluída com sucesso'
                                + '</div>';
                        setTimeout(function () {
                            $('#avisos').hide('slow');
                        }, 3000);
                    }
                }
            });
        }
        
});

$(document).ready(function () {
    $('#formProjeto').load('template/formCadastroEntidade.php');
    $('#formProjeto').hide('slow');
    
    $("#botaoAddProjeto").click(function () {
        if ($(this).hasClass("glyphicon-plus-sign")) {
            $('#formProjeto').show('slow');
            $("#numeroProjeto").mask('9?99999');
            $(this).removeClass("glyphicon-plus-sign").addClass("glyphicon-minus-sign");
        }

        else {
            document.getElementById("msgEdicao").innerHTML = '';
            $('#formProjeto').hide('slow');
            LimparCampos();
            $(this).removeClass("glyphicon-minus-sign").addClass("glyphicon-plus-sign");
        }

    });

    $('#entidades').DataTable({
        "columnDefs": [{"targets": 0, "width": "9%"}, {"targets": 2, "orderable": false, "searchable": false, "width": "10%"}],
        "order": [[0, 'asc']]
    });

    $('[data-toggle="tooltip"]').tooltip({
        placement: 'auto top', trigger: 'hover'
    });
});


