function mostraFormAtividade() {
    var nomeClasse = $('#botaoAddAtividade').attr('class');

    if (nomeClasse === "glyphicon glyphicon-plus-sign") {
        $('#formProjeto').load('template/formCadastroAtividade.php', function () {
            $('#formProjeto').show('slow');
        });

    } else if (nomeClasse === "glyphicon glyphicon-minus-sign") {
        $('#formProjeto').hide('slow');
    }
}

function mostraFormEdicao(idAtividade, nomeAtividade) {
    $('#edicao').show();
    var nomeClasse = $('#botaoAddAtividade').attr('class');

    if (nomeClasse === "glyphicon glyphicon-plus-sign") {
        $('#formProjeto').hide();
        $('#formProjeto').load('template/formCadastroAtividade.php', function () {
            $('#formProjeto').show('slow');
            $("#botaoAddAtividade").removeClass("glyphicon glyphicon-plus-sign").addClass("glyphicon glyphicon-minus-sign");
            $('#id').val(idAtividade);
            $('#nomeAtividade').val(nomeAtividade);
        });

    } else if (nomeClasse === "glyphicon glyphicon-minus-sign") {
        $('#id').val(idAtividade);
        $('#nomeAtividade').val(nomeAtividade);
    }
}

function mostraFormAcao(idAtividade) {
    $('#formAcao').load('template/formCadastroAcao.php?idAtividade=' + idAtividade, function () {
        $('#formAcao').show('slow');
        $('#editandoAcao').hide();
        initialize();
        $("#botaoAddAcao").click(function () {
            LimparCamposAcao();
            $('#editandoAcao').hide();
        });
        $("#telefone").mask('(99) 9999-9999?9');
        $('[data-toggle="tooltip"]').tooltip({
            placement: 'auto top', trigger: 'hover'
        });
        //Tempo para buscar o endereço no mapa
    });
}

function CadastraAtividade() {
    var idAtividade = $('#id').val();
    var nomeAtividade = $('#nomeAtividade').val();
    var projetoId = $('#projetoId').val();
    if (nomeAtividade == '') {
        ErroSalvar();
    } else {
        $.ajax({
            type: 'POST',
            url: 'dao/cadastroAtividadeDao.php',
            data: 'nomeAtividade=' + nomeAtividade + '&projetoId=' + projetoId + '&idAtividade=' + idAtividade,
            success: function (data) {
                if (data == 'erro') {
                    ErroSalvar();
                } else {
                    if (idAtividade === '') {
                        AdicionarRegistroAtividade(data);
                    } else {
                        EditarRegistro(data);
                        var botao = $('#botaoAddAtividade');
                        $('#formProjeto').hide('slow');
                        if (botao.hasClass("glyphicon glyphicon-plus-sign"))
                            botao.removeClass("glyphicon glyphicon-plus-sign").addClass("glyphicon glyphicon-minus-sign");
                        else
                            botao.removeClass("glyphicon glyphicon-minus-sign").addClass("glyphicon glyphicon-plus-sign");
                    }
                }
            }
        });
    }
}

function CadastraAcao(idAtividade) {
    var parceiroChave = $('#parceiroChave').val();
    var idAcao = $('#idAcao').val();
    
    var contato = $('#contato').val();
    var telefone = $('#telefone').val();
    var email = $('#email').val();
    var endereco = $('#txtEndereco').val();
    var latitude = $('#txtLatitude').val();
    var longitude = $('#txtLongitude').val();
    if (parceiroChave == '' || contato == '' || telefone == '' || email == '' || endereco == '' || latitude == '' || longitude == '') {
        ErroSalvarAcao();
    } else {
        $.ajax({
            type: 'POST',
            url: 'dao/cadastroAcaoDao.php',
            data: 'idAtividade=' + idAtividade + '&parceiroChave=' + parceiroChave + '&contato=' + contato +
                    '&telefone= ' + telefone + '&email=' + email + '&endereco=' + endereco + '&latitude=' + latitude +
                    '&longitude=' + longitude + '&idAcao=' + idAcao,
            success: function (data) {
                if (data !== 'erro') {
                    $('#editandoAcao').hide();
                    //Zerar inputs do form de ação
                    LimparCamposAcao();
                    document.getElementById('tabelaAcaoDadosCorpo').innerHTML = data;
                    $('[data-toggle="tooltip"]').tooltip({
                        placement: 'auto top', trigger: 'hover'
                    });
                    if (idAcao == '') {
                        initialize();
                        //Exibir aviso de sucesso
                        SucessoSalvarAcao();
                    } else {
                        initialize();
                        //Exibir aviso de sucesso
                        SucessoEditaAcao();
                    }
                } else {
                    ErroSalvarAcao();
                }
            }
        });
    }
}

function ExcluirAtividade(idAtividade, r) {
    $.ajax({
        type: 'GET',
        url: 'dao/cadastroAtividadeDao.php',
        data: 'id=' + idAtividade + '&cmd=excluir',
        success: function (data) {
            if (data == 'erro') {
                ErroDeletarAtividade();
            } else {
                SucessoDeletarAtividade();
                ExcluirRegistro(idAtividade);
            }
        }
    });
}

function ExcluirRegistro(atividadeId) {

    var table = $('#atividades').DataTable();
    var tr = $('#' + atividadeId).closest('tr');
    table.row(tr).remove().draw(false);
}

function ErroSalvar() {
    $('#avisos').show();
    document.getElementById("avisos").innerHTML = '<div class="alert alert-danger" id="msg">'
            + '<button type="button" class="close" data-dismiss="alert">×</button>'
            + 'Erro ao salvar atividade. Tente novamente.'
            + '</div>';
    setTimeout(function () {
        $('#avisos').hide('slow');
    }, 3000);
}
function ErroDeletarAtividade() {
    $('#avisos').show();
    document.getElementById("avisos").innerHTML = '<div class="alert alert-danger" id="msg">'
            + '<button type="button" class="close" data-dismiss="alert">×</button>'
            + 'Erro ao excluir'
            + '</div>';
    setTimeout(function () {
        $('#avisos').hide('slow');
    }, 3000);
}

function ErroCadastroClassificacao() {
    $('#avisoClassificacao').show();
    document.getElementById("avisoClassificacao").innerHTML = '<div class="alert alert-danger" id="msg">'
            + '<button type="button" class="close" data-dismiss="alert">×</button>'
            + 'Erro ao cadastrar classificação do projeto'
            + '</div>';
    setTimeout(function () {
        $('#avisoClassificacao').hide('slow');
    }, 3000);
}

function ErroSalvarAcao() {
    $('#avisosAcao').show();
    document.getElementById("avisosAcao").innerHTML = '<div class="alert alert-danger" id="msg">'
            + '<button type="button" class="close" data-dismiss="alert">×</button>'
            + 'Erro ao cadastrar ação'
            + '</div>';
    setTimeout(function () {
        $('#avisosAcao').hide('slow');
    }, 3000);
}

function SucessoDeletarAtividade() {
    $('#avisos').show();
    document.getElementById("avisos").innerHTML = '<div class="alert alert-success" id="msg">'
            + '<button type="button" class="close" data-dismiss="alert">×</button>'
            + 'Registro excluído com sucesso'
            + '</div>';
    setTimeout(function () {
        $('#avisos').hide('slow');
    }, 3000);
}

function SucessoSalvarAcao() {
    $('#avisosAcao').show();
    document.getElementById("avisosAcao").innerHTML = '<div class="alert alert-success" id="msg">'
            + '<button type="button" class="close" data-dismiss="alert">×</button>'
            + 'Ação cadastrada com sucesso'
            + '</div>';
    setTimeout(function () {
        $('#avisosAcao').hide('slow');
    }, 3000);
}

function SucessoEditaAcao() {
    $('#avisosAcao').show();
    document.getElementById("avisosAcao").innerHTML = '<div class="alert alert-success" id="msg">'
            + '<button type="button" class="close" data-dismiss="alert">×</button>'
            + 'Ação atualizada com sucesso'
            + '</div>';
    setTimeout(function () {
        $('#avisosAcao').hide('slow');
    }, 3000);
}

function EditarRegistro(atividadeId) {
    $('#atividades').dataTable().fnUpdate($('#nomeAtividade').val(), $('tr#' + atividadeId)[0], 1);
    $('#id').val("");
    $('#nomeAtividade').val("");
    $('#edicao').hide();
    $('#avisos').show();
    document.getElementById("avisos").innerHTML = '<div class="alert alert-success" id="msg">'
            + '<button type="button" class="close" data-dismiss="alert">×</button>'
            + 'Atividade alterada com sucesso'
            + '</div>';
    setTimeout(function () {
        $('#avisos').hide('slow');
    }, 3000);
}

function EditarAcao(idAcao) {
    $('#editandoAcao').show();
    var body = $('html, body');
    body.animate({scrollTop: $('#botaoAddAcao').offset().top - 60}, 'slow');
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: 'dao/cadastroAcaoDao.php',
        data: 'idAcao=' + idAcao,
        success: function (data) {
            if (data !== 'erro') {
                $('#parceiroChave').val(data[0].parceiro_chave);
                $('#contato').val(data[0].contato);
                $('#telefone').val(data[0].telefone);
                $('#email').val(data[0].email);
                $('#txtEndereco').val(data[0].endereco);
                $('#txtLatitude').val(data[0].latitude);
                $('#txtLongitude').val(data[0].longitude);
                $('#idAcao').val(idAcao);
                BuscaEnderecoMapa();
            }
        }
    });
}

function AdicionarRegistroAtividade(atividadeId) {
    var table = $('#atividades').DataTable();
    var rowNode = table.row.add(['<span class="glyphicon glyphicon-plus-sign" style="font-size: 1.7em !important;"></span>',
        $('#nomeAtividade').val(),
        '<a href="#" id="' + atividadeId + '" onclick="ExcluirAtividade(' + atividadeId + ',this)" data-toggle="tooltip" data-title="Remover atividade"><span class="btn btn-danger btn-sm glyphicon glyphicon-remove" style="font-size: 1.1em !important; border-radius: 5px 0px 0px 5px;"></span></a>\n\
        <a href="javascript:mostraFormEdicao(' + atividadeId + ',\'' + $('#nomeAtividade').val() + '\')" data-toggle="tooltip" data-title="Editar atividade"><span class="btn btn-warning btn-sm glyphicon glyphicon-pencil" style="font-size: 1.1em !important; border-radius: 0px 5px 5px 0px;"></span></a><input type="hidden" class="input-id-atividade" value="' + atividadeId + '">'])
            .draw()
            .node();
    $(rowNode).find('td').eq(0).addClass('details-control');
    $(rowNode).find('td').eq(2).css({display: "inline-table"});
    $(rowNode).find('td').eq(2).css({'white-space': "nowrap"});

    $('[data-toggle="tooltip"]').tooltip({
        placement: 'auto top', trigger: 'hover'
    });
    $('#nomeAtividade').val("");
    $('#avisos').show();
    document.getElementById("avisos").innerHTML = '<div class="alert alert-success" id="msg">'
            + '<button type="button" class="close" data-dismiss="alert">×</button>'
            + 'Atividade cadastrada com sucesso'
            + '</div>';
    setTimeout(function () {
        $('#avisos').hide('slow');
    }, 3000);
}

function ZerarFormAcao() {
    $('#parceiroChave').val('');
    $('#contato').val('');
    $('#telefone').val('');
    $('#email').val('');
    $('#txtEndereco').val('');
    $('#txtLatitude').val('');
    $('#txtLongitude').val('');
}

function LimparCamposAcao() {
    $("#formAcao").find("input[type=\"text\"]").val('');
    $("#formAcao").find("input[type=\"number\"]").val('');
    $("#formAcao").find("input:hidden").val('');
    $("#formAcao").find("select").prop('selectedIndex', 0);
    $("#formAcao").find("input[type!=hidden]:first").focus();
}


var geocoder;
var map;
var marker;
//Função para mapa com api do Google
function initialize() {

    position = {lat: -29.720877169642595, lng: -53.71489551834105};
    var mapOptions = {
        zoom: 14,
        center: position,
        mapTypeId: google.maps.MapTypeId.HYBRID
    };
    map = new google.maps.Map(document.getElementById('map'), mapOptions);

    marker = new google.maps.Marker({
        position: position,
        map: map,
        draggable: true
    });

    var infowindow = new google.maps.InfoWindow({
        content: '<p>Escritório de Projetos</p>'
    });

    google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(map, marker);
    });

    google.maps.event.addListener(marker, 'drag', function () {
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    $('#txtEndereco').val(results[0].formatted_address);
                    $('#txtLatitude').val(marker.getPosition().lat());
                    $('#txtLongitude').val(marker.getPosition().lng());
                }
            }
        });
    });
}

//Busca o endereço para cada nova ação criada
function buscarPorEnderecoNoMapa() {
    endereco = $('#txtEndereco').val();
    geocoder = new google.maps.Geocoder();
    console.log(endereco + ' RS');
    geocoder.geocode({'address': endereco + ', Brasil'}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();

                console.log("Latitude: " + latitude);

                $('#txtEndereco').val(results[0].formatted_address);
                $('#txtLatitude').val(latitude);
                $('#txtLongitude').val(longitude);

                var location = new google.maps.LatLng(latitude, longitude);
                marker.setPosition(location);
                map.setCenter(location);
                map.setZoom(16);
            }
        }
    });
}

//Utilizada para editar a partir de coordenadas já cadastradas
function carregarNoMapa(endereco, latitude, longitude) {
    geocoder = new google.maps.Geocoder();
    //console.log(endereco + ' RS');
    geocoder.geocode({'address': endereco + ', Brasil'}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                //var latitude = results[0].geometry.location.lat();
                //var longitude = results[0].geometry.location.lng();

                console.log("Latitude: " + latitude);

                $('#txtEndereco').val(results[0].formatted_address);
                $('#txtLatitude').val(latitude);
                $('#txtLongitude').val(longitude);

                var location = new google.maps.LatLng(latitude, longitude);
                marker.setPosition(location);
                map.setCenter(location);
                map.setZoom(16);
            }
        }
    });
}

function BuscaEnderecoMapa() {
    console.log("endereco");
    //Comentado pois buscava por endereço
    //if ($("#txtEndereco").val() != "")
    //carregarNoMapa($("#txtEndereco").val());
    if ($("#txtEndereco").val() != "" && $("#txtLatitude").val() != "" && $("#txtLongitude").val() != "") {
        carregarNoMapa($("#txtEndereco").val(), $("#txtLatitude").val(), $("#txtLongitude").val());
    }
}

$(document).ready(function () {
    $('#edicao').hide();
    $('#formAcao').hide();
    $('[data-toggle="tooltip"]').tooltip({
        placement: 'auto top', trigger: 'hover'
    });

    var table = $('#atividades').DataTable({
        "columnDefs": [
            {"targets": 0, "orderable": false, "searchable": false, "width": "5%"},
            {"targets": 2, "orderable": false, "searchable": false, "width": "5%"}],
        "order": [[1, 'asc']]
    });

    $("#ajudaClassificacao").click(function () {
        
    });
    
    $("#botaoAddAtividade").click(function () {
        mostraFormAtividade();
        var botao = $(this);
        $('#edicao').hide();
        if (botao.hasClass("glyphicon glyphicon-plus-sign"))
            botao.removeClass("glyphicon glyphicon-plus-sign").addClass("glyphicon glyphicon-minus-sign");
        else
            botao.removeClass("glyphicon glyphicon-minus-sign").addClass("glyphicon glyphicon-plus-sign");
    });

    $('#atividades tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var idAtividade = tr.find('.input-id-atividade').val();
        var row = table.row(tr);
        var glyphicon = $(this).find('span');

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
            glyphicon.removeClass('glyphicon-minus-sign').addClass('glyphicon-plus-sign');
        }
        else {
            // Open this row
            $.ajax({
                type: 'POST',
                url: 'dao/cadastroAcaoDao.php',
                data: 'idAtividade=' + idAtividade,
                success: function (data) {
                    if (data === null) {
                        alert('Não foi possível carregar "Ações"');
                    } else {
                        table.rows('.shown').eq(0).each(function (tr) {
                            var rowAberta = table.row(tr);

                            if (rowAberta.child.isShown()) {
                                rowAberta.child.hide();
                                var td = $('#atividades tbody').find('td.details-control');
                                var icone = td.find('span');
                                icone.removeClass('glyphicon-minus-sign').addClass('glyphicon-plus-sign');
                            }
                        });
                        row.child(data).show();
                        mostraFormAcao(idAtividade);
                        var body = $('html, body');
                        body.animate({scrollTop: $('#formAcao').offset().top - 147, }, 'fast');
                        tr.addClass('shown');
                        glyphicon.removeClass('glyphicon-plus-sign').addClass('glyphicon-minus-sign');
                    }
                }
            });
        }
    });

    $('.checkbox-classificacao').change(function () {
        var areaAtuacao = $(this).val();
        var numeroProjeto = $('#inputNumProjeto').val();
        $.ajax({
            type: 'POST',
            url: 'dao/identificacaoDao.php',
            data: 'areaAtuacao=' + areaAtuacao + '&numeroProjeto=' + numeroProjeto,
            success: function (data) {
                if (data === 'erro') {
                    ErroCadastroClassificacao();
                }
            }
        });
    });


});