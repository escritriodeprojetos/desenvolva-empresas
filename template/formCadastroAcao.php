<?php
if (!empty($_GET['idAtividade'])) {
    echo '<input value="' . $_GET['idAtividade'] . '" id="idAtividade" type="hidden">';
}
?>
<input type="hidden" id="idAcao" value="">
<form id="formAcao">
    <div id="avisosAcao"></div>
    <div class="row">
        <div class="col-md-5 col-sm-12">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <label for="parceiroChave">Parceiro-chave</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input style="width: 100% !important;" id="parceiroChave" type="text" name="parceiroChave" required="" placeholder="Parceiro-chave" class="form-control"> 
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 7px;">
                        <label for="contato">Contato</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input style="width: 100%;" id="contato" type="text" name="contato" required="" placeholder="Contato" class="form-control acao"> 
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 7px;">
                        <label for="telefone">Telefone</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input style="width: 100%;" id="telefone" type="text" name="telefone" required="" placeholder="Telefone" class="form-control"> 
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 7px;">
                        <label for="email">Email</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input style="width: 100%;" id="email" type="text" name="email" required="" placeholder="Email" class="form-control" autocomplete="off"> 
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 7px;">
                        <label for="txtEndereco">Endereço</label><br><i>Clique em "Buscar" para mapear o endereço.</i>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="input-group" style="display: inline-flex; width: 100%"><input style="width: 100%;" type="text" id="txtEndereco" placeholder="Endereço" class="form-control"><button type="button" class="btn btn-default  input-group-addon" onclick="buscarPorEnderecoNoMapa()">Buscar</button></div>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 7px;">
                        <label for="txtLatitude">Latitude</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input style="width: 100%;" type="text" id="txtLatitude" placeholder="Latitude" readonly="" class="form-control">
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 7px;">
                        <label for="txtLongitude">Longitude</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input style="width: 100%;" type="text"   id="txtLongitude" placeholder="Longitude" readonly="" class="form-control">
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 4%; text-align: right;">
                        <button onclick="LimparCamposAcao()" class="btn btn-default">Limpar campos</button>  
                        <!--fazer func para cadastrar ação-->
                        <?php
                        echo '<a class="btn btn-primary" onclick="CadastraAcao(' . $_GET['idAtividade'] . ')">Salvar</a>'
                        ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-7 col-sm-12" style="min-height: 400px; margin-top: 24px;">
            <div id="map" style="height: 400px; width: 100%;"></div>                                
        </div>
    </div>
</form>
