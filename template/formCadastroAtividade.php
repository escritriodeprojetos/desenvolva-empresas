<div class="col-md-5 col-sm-12">
    <form id="ajaxForm">
        <div class="form-group"> 
            <input type="hidden" name="id" id="id">
            <label>Nome</label> 
            <input id="nomeAtividade" type="text" name="nomeAtividade" required placeholder="Nome da Atividade" class="form-control"> 
        </div> 
        <div class="form-group" style="text-align: right; padding-bottom: 20px"> 
            <button type="reset" class="btn btn-default" id="buttonReset">Limpar campos</button>  
            <a class="btn btn-primary" id="salvar" onclick="CadastraAtividade()">Salvar</a> 
        </div> 
    </form> 
</div> 
