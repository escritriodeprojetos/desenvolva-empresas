<div class="modal fade" role="dialog" id="creditos">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Créditos</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <center>
                            <h4>Bruno Frizzo Trojahn</h4>
                            <p>
                                <img src="img/brunof.jpg" alt="Bruno Frizzo" style="width: 170px; height: 170px;" class="img-circle"/>
                            </p>
                            <p><i class="glyphicon glyphicon-envelope"></i> <a href="mailto:#">brunofrizzo@outlook.com</a></p>
                        </center>
                    </div>
                    <div class="col-md-6">
                        <center>
                            <h4>Fernando Quatrin Campagnolo</h4>
                            <p>
                                <img src="img/fernandoq.jpg" alt="Fernando Campagnolo" style="width: 170px; height: 170px;" class="img-circle"/>
                            </p>
                            <p><i class="glyphicon glyphicon-envelope"></i> <a href="mailto:#">fcampagnolo@inf.ufsm.br</a></p>
                        </center>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <center>
                            <h4>Fernando Vedoin Garcia</h4>
                            <p>
                                <img src="img/fernandog.jpg" alt="Fernando Garcia" style="width: 170px; height: 170px;" class="img-circle"/>
                            </p>
                            <p><i class="glyphicon glyphicon-envelope"></i> <a href="mailto:#">fvedoingarcia@gmail.com</a></p>
                        </center>
                    </div>
                    <div class="col-md-6">
                        <center>
                            <h4>Luis Henrique Medeiros</h4>
                            <p>
                                <img src="img/zilly.jpg" alt="Luis Henrique Medeiros" style="width: 170px; height: 170px;" class="img-circle"/>
                            </p>
                            <p><i class="glyphicon glyphicon-envelope"></i> <a href="mailto:#">lhmedeiros@hotmail.com</a></p>
                        </center>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <center>
                            <h4>Miguel Bauerman Brasil</h4>
                            <p>
                                <img src="img/miguel.gif" alt="Miguel Brasil" style="width: 170px; height: 170px;" class="img-circle"/>
                            </p>
                            <p><i class="glyphicon glyphicon-envelope"></i> <a href="mailto:#">miguelbrasil@ctism.ufsm.br</a></p>
                        </center>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Sair</button> 
            </div>
        </div>
    </div>
</div>

