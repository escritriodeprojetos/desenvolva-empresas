<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <script type="text/javascript">
    function RedirecionarPagina() {
        window.location.href = "index.php";
    }    
    </script>
        
    <body>
        <div id="modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Aviso</h4>
                    </div>
                    <div class="modal-body">
                        <p>Cadastro realizado com sucesso.</p>
                        <p>Em breve um e-mail será enviado para <strong style="color: #006dcc"><?php echo $_GET['email'] ?></strong> para a ativação do cadastro.</p>
                        <p>OBS: Se o e-mail não estiver na sua caixa de entrada, verifique a pasta de Spam.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="RedirecionarPagina()">Sair</button> 
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
