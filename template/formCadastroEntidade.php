<div class="col-md-5 col-sm-12">
    <input type="hidden" name="id" id="idProjeto">
    <div class="form-group"> 
        <label>Número</label> 
        <input id="numeroProjeto" type="text" name="numeroProjeto" required="" placeholder="Número do Projeto" class="form-control">
    </div> 
    <div class="form-group"> 
        <label>Nome</label> 
        <input id="nomeProjeto" type="text" name="nomeProjeto" required="" placeholder="Nome do Projeto" class="form-control"> 
    </div> 
    <div class="form-group"> 
        <label>Tipo</label><br> 
        <input type="radio" name="tipo" value="Ensino"> Ensino     
        <input type="radio" name="tipo" value="Pesquisa"> Pesquisa     
        <input type="radio" name="tipo" value="Extensão"> Extensão     
        <input type="radio" name="tipo" value="Institucional"> Institucional
    </div> 
    <div class="form-group" style="text-align: right; padding-bottom: 20px"> 
        <button onclick="LimparCampos()" class="btn btn-default">Limpar campos</button>  
        <a class="btn btn-primary" onclick="CadastrarProjeto()">Salvar</a> 
    </div>
</div> 
