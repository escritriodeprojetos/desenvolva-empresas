<div class="modal fade" role="dialog" id="modalClassificacao">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Ajuda</h4>
            </div>
            <div class="modal-body">
                <ul>
                    <li>
                        Aqui você pode selecionar as áreas de atuação de seu projeto.
                    </li>
                    <li>
                        Ao clicar na caixa de seleção, a área correspondente será salva automaticamente. Caso contrário, desmarque-a.
                    </li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Sair</button> 
            </div>
        </div>
    </div>
</div>

