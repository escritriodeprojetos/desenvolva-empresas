<div class="modal fade" role="dialog" id="modalAtividades">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Ajuda</h4>
            </div>
            <div class="modal-body">
                <ul>
                    <li>
                        Aqui você pode gerenciar suas atividades e as ações correspondentes. 
                    </li>
                    <li>
                        O ícone "<i class="glyphicon glyphicon-plus-sign"></i>" possibilitará o cadastro de novas atividades ou a visualização de ações de determinada atividade.
                    </li>
                    <li>
                        Para excluir ou alterar algum registro, use os botões localizados à direita na tabela.
                    </li>
                    <li>
                        Também é possível visualizar os dados de cada ação por meio do botão "<i class="glyphicon glyphicon-search"></i>"
                    </li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Sair</button> 
            </div>
        </div>
    </div>
</div>

