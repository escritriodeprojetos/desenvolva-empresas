<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <?php
        ob_start();
        include_once 'util/autentica.php';
        include_once 'template/scripts.php';
        include_once 'model/Usuario.php';
        include_once 'model/Entidade.php';
        include_once 'model/Atividade.php';
        include_once 'model/Area.php';
        include_once 'model/AreaAtuacao.php';

        //Implementar verificações adicionais
        if (empty($_GET['numeroProjeto'])) {
            header('location: projetos.php');
        }

        $usuario = new Usuario();
        $usuario->BuscaById($_SESSION['usuario_id']);

        $projeto = new Entidade();
        $projeto->BuscaByNumeroByUsuarioId($_GET['numeroProjeto'], $usuario->getId());

        if ($projeto->getUsuarioId() != $usuario->getId()) {
            header('location: projetos.php');
        }
        ?>
        <link href="css/dataTables.css" rel="stylesheet" media="screen">
        <title>Identificação de Projetos - Portal Desenvolva</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-default navbar-fixed-top">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <span class="navbar-brand">Portal Desenvolva</span>
                            </div>
                            <div class="collapse navbar-collapse" id="myNavbar">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="entidades.php"><span class="glyphicon glyphicon-circle-arrow-left gi-2-1x"></span> Entidades</a></li>
                                    <li><a href="#" data-toggle="modal" data-target="#creditos"><span class="glyphicon glyphicon-info-sign gi-2-1x"></span> Créditos</a></li>
                                    <li><a href="util/logout.php"><span class="glyphicon glyphicon-log-out"></span> Sair</a></li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            <div id="modal">
                <?php
                    include_once 'template/modalCreditos.php';
                    include_once 'template/modalClassificacao.php';
                    include_once 'template/modalAtividades.php';
                ?>
            </div>
            <fieldset class="form-group" style="margin-top: 75px">
                <legend><?php echo $projeto->getNome() . ' - <span id="numeroProjeto">' . $projeto->getNumero() . '</span>' ?></legend>
                <input type="hidden" id="inputNumProjeto" value="<?php echo $projeto->getNumero() ?>"></input>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading clearfix">
                                <i class="icon-calendar"></i>
                                <h3 class="panel-title">
                                    Classificação 
                                    <a href="#" class="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalClassificacao" style="text-decoration: none; float: right; color: #337ab7; font-size: 1.3em;"></a>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="row" id="avisoClassificacao"></div>
                                </div>
                                <?php
                                $areaAtuacao = new AreaAtuacao();
                                $areaObj = new Area();
                                $listaAreas = $areaObj->BuscaTodos();
                                $areasAtuacao = $areaAtuacao->BuscaByProjetoIdArray($projeto->getId());
                                foreach ($listaAreas as $area) {
                                    if (!empty($area['id']) && !empty($areasAtuacao)) {
                                        $isChecked = ( (in_array($area['id'], $areasAtuacao)) ? "checked" : "");
                                    } else {
                                        $isChecked = "";
                                    }
                                    echo '<div class="col-md-3 col-sm-3">
                                        <label class="checkbox-inline">
                                        <input type="checkbox" value="' . $area['id'] . '"
                                            class="checkbox-classificacao" ' . $isChecked . '>' . $area['nome'] . '
                                        </label>
                                    </div>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading clearfix">
                                <i class="icon-calendar"></i>
                                <h3 class="panel-title">
                                    Atividades 
                                    <button href="#" class="glyphicon glyphicon-plus-sign" id="botaoAddAtividade" style="text-decoration: none; color: #337ab7; font-size: 1.3em !important; background: transparent; border: 0px;"></button>
                                    <span id="edicao" style="color: red;"> - Em edição</span>
                                    <a href="#" id="ajudaAtividade" class="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAtividades" style="float: right; color: #337ab7; font-size: 1.3em; text-decoration: none"></a>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <input type="hidden" id="projetoId" value="<?php echo $projeto->getId() ?>">
                                <div class="col-md-12">
                                    <div class="row" id="avisos"></div>
                                </div>
                                <div class="row" id="formProjeto"></div>
                                <table id="atividades" class="table table-striped table-bordered table-condensed table-hover display" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Atividades</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $atividade = new Atividade();
                                        $atividades = $atividade->BuscaByProjetoId($projeto->getId());
                                        if (!empty($atividades)) {
                                            foreach ($atividades as $result) {
                                                echo '<div id="idAtividade" style="display: none;">' . $result['id'] . '</div>';
                                                echo '<tr id="' . $result['id'] . '">'
                                                . '<td class="details-control"><span class="glyphicon glyphicon-plus-sign" style="font-size: 1.7em !important;"></span></td>'
                                                . '<td>' . $result['nome'] . '</td>'
                                                . '<td style="white-space: nowrap; display: inline-table"><a href="#" id="' . $result['id'] . '" onclick="ExcluirAtividade(' . $result['id'] . ',this)" data-toggle="tooltip" data-title="Remover atividade"><span class="btn btn-danger btn-sm glyphicon glyphicon-remove" style="font-size: 1.1em !important; border-radius: 5px 0px 0px 5px;"></span></a>'
                                                . ' <a href="javascript:mostraFormEdicao(' . $result['id'] . ',\'' . $result['nome'] . '\')" data-toggle="tooltip" data-title="Editar atividade"><span class="btn btn-warning btn-sm glyphicon glyphicon-pencil" style="font-size: 1.1em !important; border-radius: 0px 5px 5px 0px;"></span></a>
                                                </td>
                                                <input type="hidden" class="input-id-atividade" value="' . $result['id'] . '">
                                                </tr>';
                                            }
                                        }
                                        ?>                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset> 
        </div>
        <script type="text/javascript" class="init" src="js/identificacao.php.js"></script>
    </body>
</html>

