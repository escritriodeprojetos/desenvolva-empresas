<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <?php
        include_once 'template/scripts.php';
        ?>
        <script type="text/javascript" class="init">
            function verificaSenha() {
                var senha1 = document.getElementById('senha1');
                var senha2 = document.getElementById('senha2');
                var mensagem = document.getElementById('mensagemConfirmacao');
                var goodColor = "#66cc66";
                var badColor = "#ff6666";

                if (senha1.value === senha2.value) {
                    senha2.style.borderColor = goodColor;
                    mensagem.style.color = goodColor;
                    mensagem.innerHTML = "Senhas coincidem!"
                } else {
                    senha2.style.borderColor = badColor;
                    mensagem.style.color = badColor;
                    mensagem.innerHTML = "Senhas não coincidem!"
                }
            }

            $(".alert").alert('close');

            $(document).ready(function () {
                setTimeout(function () {
                    $('.fadeAlert').fadeOut('slow');
                }, 5000);

                $('#modal').modal();
                $('[data-toggle="tooltip"]').tooltip({
                    placement: 'auto top', trigger: 'hover'
                });
            });
        </script>
        <title>Cadastro de Coordenadores de Projetos - Portal Desenvolva</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-default navbar-fixed-top">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <span class="navbar-brand">Portal Desenvolva</span>
                            </div>
                            <div class="collapse navbar-collapse" id="myNavbar">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="index.php"><span class="glyphicon glyphicon-home"></span> Início</a></li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            <?php
            if (!empty($_GET['msg']) && $_GET['msg'] === 'sucesso' && !empty($_GET['email'])) {
                require_once 'template/modalCadastroUsuario.php';
            }
            ?>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <form action="dao/cadastroUsuarioDao.php" method="post" >
                        <fieldset style="margin-top: 75px; width: 100%;">
                            <?php
                            if (!empty($_GET['msg']) && $_GET['msg'] === 'expirado') {
                                echo '<div class="alert alert-warning" id="msg">'
                                . '<button type="button" class="close" data-dismiss="alert">×</button>'
                                . 'Prazo para ativação da conta expirado. Para reenviar o email de ativação, clique <a href="cadastroUsuario.php?msg=reenviaEmail&siape='.$_GET['siape'].'">aqui</a>'
                                . '</div>';
                            }else if (!empty ($_GET['msg']) && $_GET['msg'] === 'reenviaEmail'){
                                include_once 'model/Usuario.php';
                                $usuario = new Usuario();
                                $usuario->setSiape($_GET['siape']);
                                $usuario->EnviarEmailAtivacaoContaNew();
                                echo '<div class="alert alert-success" id="msg">'
                                . '<button type="button" class="close" data-dismiss="alert">×</button>'
                                . 'E-mail enviado com sucesso! Verifique sua caixa de entrada e spam no prazo de 1 dia.'
                                . '</div>';
                            }
                            ?>
                            <legend>Cadastro de Coordenadores de Projetos</legend>
                            <div class="form-group">
                                <label>Nome Completo</label>
                                <input type="text" name="nome" required="" placeholder="Digite seu nome" class="form-control">
                            </div> 
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" name="email" required="" placeholder="Digite seu e-mail" class="form-control">
                                <?php
                                if (!empty($_GET['msg']) && $_GET['msg'] === 'email') {
                                    echo '<span style="color: red" class="fadeAlert">E-mail já cadastrado no sistema</span>';
                                }
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Senha</label></td>
                                <input type="password" name="senha" required="" placeholder="Digite sua senha" class="form-control" id="senha1">
                            </div>   
                            <div class="form-group">
                                <label>Confirme sua senha</label>
                                <input type="password" name="senha2" required="" placeholder="Digite novamente sua senha" class="form-control" id="senha2" onkeyup="verificaSenha();
                                        return false;">
                                <span id="mensagemConfirmacao"></span>
                                <?php
                                if (!empty($_GET['msg']) && $_GET['msg'] === 'senha') {
                                    echo '<span style="color: red" class="fadeAlert">Senhas não conferem.</span>';
                                }
                                ?>
                            </div>
                            <div class="form-group" style="text-align: right">
                                <p style="text-align: right">OBS: Todos os campos devem ser preenchidos.</p>
                                <button type="reset" class="btn btn-default">Limpar campos</button>
                                <button type="submit" class="btn btn-primary">Salvar</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <!--<div class="col-md-7 col-sm-7 col-xs-7"></div>-->
            </div>
            <!--</fieldset>-->
        </div>
    </body>
</html>
